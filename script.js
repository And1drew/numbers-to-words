const numberSpan = document.getElementById("NumberSpan")
const ones = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"];
const teens = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"];
const tens = ["ten", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninty"];
const hundreds = ["onehundred ", "twohundred ", "threehundred ", "fourhundred ", "fivehundred ", "sixhundred ", "sevenhundred ", "eighthundred ", "ninehundred ","one Thousand"];

function wordsToHundred() {
    let words = [];
    for (let i = 0; i <= 100; i++) {
        if (i < 9) { words.push(ones[i]); }
        if ((i >= 9) && (i < 19)) { words.push(teens[i - 9]); }
        if ((i >= 19) && (i < 100)) {
            words.push(tens[Math.floor((i - 9) / 10)]);
            if ((i - 9) % 10 != 0) {words[i] = words[i].concat(ones[((i - 9) % 10) - 1])}
        }
        if(i === 98 ){words.push(hundreds[0])}
    }
    words.pop();
    return words;
}

function wordsToThousand(X){
    let words = wordsToHundred();
    for (let j = 0; j < 99; j++){
        words[j] = hundreds[X].concat(words[j])
    }
    words.pop();
    words.push(hundreds[X+1])
    return words;
}
let words = [];
words += wordsToHundred();
words += wordsToThousand(0);
words += wordsToThousand(1);
words += wordsToThousand(2);
words += wordsToThousand(3);
words += wordsToThousand(4);
words += wordsToThousand(5);
words += wordsToThousand(6);
words += wordsToThousand(7);
words += wordsToThousand(8);

document.body
numberSpan.innerText=words